package space.devport.wertik.armordur.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import space.devport.wertik.armordur.Main;

public class ArmorDurCmd implements CommandExecutor {

    private Main plugin;

    public ArmorDurCmd() {
        plugin = Main.getInstance();
    }

    private void help(CommandSender s) {
        s.sendMessage("§8§m        §c Armor Durability §8§m        " +
                "\n§c/ad credits §8- §7Display credits & contacts." +
                "\n§c/ad debug §8- §7Switch player debug mode. §c[§4! §7Experimental§c]" +
                "\n§c/ad reload §8- §7Reload the plugin.");
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            help(sender);
            return true;
        }

        if (!sender.hasPermission("armordur.admin")) {
            sender.sendMessage("§cYou lack thee permission!");
            return true;
        }

        if (sender.hasPermission("spawners.admin"))
            switch (args[0].toLowerCase()) {
                case "credits":
                    sender.sendMessage("§8§m        §c Armor Durability §7v.§f" + plugin.getDescription().getVersion() + " §8§m        " +
                            "\n§7Proudly built in §3The Port §8@ §7https://devport.space \n        §8by §3Wertík §7( Wertík#8849 )");
                    break;
                case "debug":
                    Main.getInstance().cO.switchDebug(sender);
                    sender.sendMessage("§eSwitched debug mode. §c[§4!§c] §7Experimental feature");
                    break;
                case "reload":
                    plugin.reload(sender);
                    break;
            }

        return true;
    }
}