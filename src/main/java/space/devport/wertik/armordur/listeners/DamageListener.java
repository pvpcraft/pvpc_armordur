package space.devport.wertik.armordur.listeners;

import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.bukkit.RegionQuery;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import space.devport.wertik.armordur.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class DamageListener implements Listener {

    private Main plugin;

    private HashMap<UUID, List<ItemStack>> cache;

    public DamageListener() {
        plugin = Main.getInstance();

        cache = new HashMap<>();
    }

    @EventHandler
    public void onA(PlayerItemDamageEvent e) {
        if (cache.containsKey(e.getPlayer().getUniqueId())) {
            List<ItemStack> items = cache.get(e.getPlayer().getUniqueId());

            if (items.contains(e.getItem())) {
                e.setCancelled(true);

                items.remove(e.getItem());
            }

            if (items.isEmpty())
                cache.remove(e.getPlayer().getUniqueId());
            else
                cache.put(e.getPlayer().getUniqueId(), items);
        }
    }

    @EventHandler
    public void onTouch(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            if (!(e.getDamager() instanceof Player)) {
                // Player damaged by entity
                Player player = (Player) e.getEntity();
                Location loc = player.getLocation();

                RegionContainer container = WorldGuardPlugin.inst().getRegionContainer();
                RegionQuery query = container.createQuery();

                ApplicableRegionSet set = query.getApplicableRegions(loc);

                if (set.testState(null, Main.IGNORE_ENTITY_DUR_LOSS)) {
                    List<ItemStack> items = new ArrayList<>();

                    if (cache.containsKey(player.getUniqueId()))
                        items = cache.get(player.getUniqueId());

                    if (player.getInventory().getHelmet() != null)
                        items.add(player.getInventory().getHelmet());

                    if (player.getInventory().getChestplate() != null)
                        items.add(player.getInventory().getChestplate());

                    if (player.getInventory().getLeggings() != null)
                        items.add(player.getInventory().getLeggings());

                    if (player.getInventory().getBoots() != null)
                        items.add(player.getInventory().getBoots());

                    cache.put(player.getUniqueId(), items);
                }
            }
        }
    }
}
