package space.devport.wertik.armordur;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import space.devport.wertik.armordur.commands.ArmorDurCmd;
import space.devport.wertik.armordur.listeners.DamageListener;
import space.devport.wertik.armordur.util.Configuration;
import space.devport.wertik.armordur.util.ConsoleOutput;

public class Main extends JavaPlugin {

    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

    private Configuration config;

    public ConsoleOutput cO;

    public static StateFlag IGNORE_ENTITY_DUR_LOSS;

    @Override
    public void onEnable() {
        instance = this;

        config = new Configuration(this, "config");

        cO = new ConsoleOutput(this);
        cO.setDebug(config.getYaml().getBoolean("debug-enabled", false));
        cO.setPrefix(config.getColored("plugin-prefix"));

        getCommand("armordurability").setExecutor(new ArmorDurCmd());
        getServer().getPluginManager().registerEvents(new DamageListener(), this);
    }

    @Override
    public void onLoad() {
        FlagRegistry registry = WorldGuardPlugin.inst().getFlagRegistry();
        try {
            StateFlag flag = new StateFlag("ignore-entity-dur-loss", false);

            registry.register(flag);
            IGNORE_ENTITY_DUR_LOSS = flag;
        } catch (FlagConflictException e) {
            Flag<?> existing = registry.get("ignore-entity-dur-loss");

            if (existing instanceof StateFlag) {
                IGNORE_ENTITY_DUR_LOSS = (StateFlag) existing;
            } else {
                cO.err("Yeah, that flag is already registered.. weird tho.");
            }
        }
    }

    public void reload(CommandSender s) {
        long start = System.currentTimeMillis();

        cO.setReloadSender(s);

        cO.info("Reloading..");

        config.reload();

        cO.setDebug(config.getYaml().getBoolean("debug-enabled", false));
        cO.setPrefix(config.getColored("plugin-prefix"));

        cO.setReloadSender(null);

        long stop = System.currentTimeMillis();

        s.sendMessage("§aDone.. reload took " + (stop - start) + "ms.");
    }

    @Override
    public void onDisable() {

    }
}
